# Homologação DL - Restoque - Le Lis Blanc - Salesforce - puppeteer

Automação de parte da homologação de implementação de dataLayer da Restoque / Le Lis Blanc, plataforma Salesforce.

Esse script executa o puppeteer validando alguns eventos (pushes) de dataLayer.
Planilha com o plano de pushes de dataLayer: https://docs.google.com/spreadsheets/d/1RSvhSRAcO7V0fhJOcwHoJDEfP7cxWNMrEInXT_BfMJ4/edit#gid=320034059

## Rodando o puppeteer

```
git clone https://gitlab.com/lcrespilho/homologacao-dl-restoque-le-lis-blanc-salesforce-puppeteer.git
cd homologacao-dl-restoque-le-lis-blanc-salesforce-puppeteer
npm install
npm run test
```

## Resultado

O resultado aparece no próprio console/terminal, como saída do jest.
Alternativamente, na pasta `reports` é gerado um relatório HTML com as mesmas informações do terminal.