const {
  login,
  listProducts_DOM,
  product_pdp_DOM,
  pages,
  selectors,
  close_aware,
  close_newsletter,
} = require('../utils');
const { DatalayerHelper, emulateDesktop, emulateMobile } = require('@lcrespilho/puppeteer-utils');
const puppeteer = require('puppeteer');

jest.setTimeout(3000000);

describe('DL HML 4', () => {
  /** @type {DatalayerHelper} */
  let dlHelper;
  const visitor_202201071 = {
    id: 'cddpcUj4JeYaXUb3gWTEkjWLVA',
    guest: false,
    email: 'tags.tests.202201071@raccoon.ag',
    firstname: 'Raccoon5',
    lastname: 'Dev Tests5',
    dateOfBirth: '30-12-1987',
    newBuyer: expect.any(Boolean),
    phone: '(11) 98798-7987',
    gender: 'Female',
    city: 'São Carlos',
    state: 'SP',
    cep: '13566-290',
    device: 'Desktop',
  };

  beforeAll(async () => {
    await page.setDefaultTimeout(180000);
    await page.waitForTimeout(400);
    await page.evaluateOnNewDocument(() => {
      window.isProductAvailable = async id => {
        return fetch(
          `${
            location.origin
          }/on/demandware.store/Sites-le_lis_blanc-Site/pt_BR/Product-ShowQuickView?pid=${encodeURIComponent(id)}`,
          {
            headers: {
              accept: 'application/json, text/javascript, */*; q=0.01',
            },
            body: null,
            method: 'GET',
            mode: 'cors',
            credentials: 'include',
          }
        )
          .then(response => response.json())
          .then(json => json.product.available)
          .catch(() => false);
      };
    });
    dlHelper = await new DatalayerHelper(page);
  });

  it('pageView na Home, deslogado, desktop', async () => {
    await emulateDesktop(page);
    await page.goto(pages.home, { waitUntil: 'networkidle0' });
    await close_aware(page);
    await close_newsletter(page);
    await page.waitForTimeout(1000);
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'home',
      visitor: {
        device: 'Desktop',
      },
      aba: 'Le Lis Blanc',
      performanceNow: expect.any(Number),
    });
  });

  it('pageView na Home, deslogado, mobile', async () => {
    await page.goto('about:blank');
    await emulateMobile(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.home, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'home',
      visitor: {
        device: 'Mobile',
      },
      aba: 'Le Lis Blanc',
      performanceNow: expect.any(Number),
    });
  });

  it('efetua login para os próximos testes', async () => {
    await login(page, 'tags.tests.202201071@raccoon.ag', 'tags.tests.202201071@raccoon.ag');
  });

  it('pageView na Home, logado, desktop', async () => {
    await page.goto('about:blank');
    await emulateDesktop(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.home, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'home',
      visitor: visitor_202201071,
      aba: 'Le Lis Blanc',
      performanceNow: expect.any(Number),
    });
  });

  it('pageView na Home, logado, mobile', async () => {
    await page.goto('about:blank');
    await emulateMobile(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.home, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'home',
      visitor: Object.assign({}, visitor_202201071, { device: 'Mobile' }),
      aba: 'Le Lis Blanc',
      performanceNow: expect.any(Number),
    });
  });

  it('pageView na Home Casa, logado, desktop', async () => {
    await page.goto('about:blank');
    await emulateDesktop(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.home_casa, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'home',
      visitor: visitor_202201071,
      aba: 'Le Lis Blanc Casa',
      performanceNow: expect.any(Number),
    });
  });

  it('pageView na Home Casa, logado, mobile', async () => {
    await page.goto('about:blank');
    await emulateMobile(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.home_casa, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'home',
      visitor: Object.assign({}, visitor_202201071, { device: 'Mobile' }),
      aba: 'Le Lis Blanc Casa',
      performanceNow: expect.any(Number),
    });
  });

  it('pageView em Search, logado', async () => {
    await page.goto('about:blank');
    await emulateDesktop(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.search, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    const listProducts = await listProducts_DOM(page, selectors.search.listProducts, 'search', expect);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'search',
      visitor: visitor_202201071,
      searchTerm: 'saia couro',
      aba: 'Le Lis Blanc',
      listProducts,
      performanceNow: expect.any(Number),
    });
  });

  it('pageView em Search, logado, escolha de filtros que alteram os produtos listados', async () => {
    // Escolha do primeiro filtro: { "Cor": 'Preto" }
    dlHelper.clearDatalayer();
    await page.click(selectors.search.filtrar);
    await page.waitForTimeout(2000);
    await page.click(selectors.search.filtrar_preto);
    await page.waitForNetworkIdle();
    let pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    let listProducts = await listProducts_DOM(page, selectors.search.listProducts, 'search', expect);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'search',
      visitor: visitor_202201071,
      searchTerm: 'saia couro',
      aba: 'Le Lis Blanc',
      listProducts,
      performanceNow: expect.any(Number),
      filters: [{ Cor: 'Preto' }],
    });

    // Escolha do segundo filtro: { "Coleção": "aquarela" }
    dlHelper.clearDatalayer();
    await page.click(selectors.search.filtrar_aquarela);
    await page.waitForTimeout(2000);
    await page.waitForNetworkIdle();
    pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    listProducts = await listProducts_DOM(page, selectors.search.listProducts, 'search', expect);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'search',
      visitor: visitor_202201071,
      searchTerm: 'saia couro',
      aba: 'Le Lis Blanc',
      listProducts,
      performanceNow: expect.any(Number),
      filters: { Cor: 'Preto', Colecao: 'aquarela' },
    });
  });

  it('pageView em Category, logado', async () => {
    await page.goto('about:blank');
    await emulateDesktop(page);
    dlHelper.clearDatalayer();
    await page.goto(pages.category, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    const listProducts = await listProducts_DOM(page, selectors.category.listProducts, 'category', expect);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      pageType: 'category',
      pageCategory: 'Moda/New',
      visitor: visitor_202201071,
      aba: 'Le Lis Blanc',
      listProducts,
      performanceNow: expect.any(Number),
    });
  });

  pages.products.forEach((productUrl, idx) => {
    it(`pageView em PDP, logado, produto ${idx + 1}`, async () => {
      await page.goto('about:blank');
      await emulateDesktop(page);
      dlHelper.clearDatalayer();
      await page.goto(productUrl, { waitUntil: 'networkidle0' });
      const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
      expect(pageViews.length).toBe(1);
      const product = await product_pdp_DOM(page, expect);
      expect(pageViews[0]).toEqual({
        event: 'pageView',
        visitor: visitor_202201071,
        pageType: 'product',
        aba: idx === 1 ? 'Le Lis Blanc Casa' : 'Le Lis Blanc',
        listProducts: [product],
        performanceNow: expect.any(Number),
      });
    });
  });

  // A propriedade aba foi corrigida. Os erros que ainda permanecem já foram apontados em outros testes. Por conta disso, desativei esse aqui.
  it.skip('pageView no fluxo Moda->Casa, para validar a propriedade "aba"', async () => {
    await page.goto('about:blank');
    await emulateDesktop(page);
    await page.goto(pages.home, { waitUntil: 'networkidle0' });
    dlHelper.clearDatalayer();
    await page.goto(pages.petit_acessorios_casa, { waitUntil: 'networkidle0' });
    const pageViews = dlHelper.getDatalayerEvent('pageView').filter(dl => !dl.tagName);
    expect(pageViews.length).toBe(1);
    expect(pageViews[0]).toEqual({
      event: 'pageView',
      visitor: visitor_202201071,
      pageType: 'category',
      pageCategory: expect.any(String),
      aba: 'Le Lis Blanc Casa',
      listProducts: await listProducts_DOM(page, selectors.category.listProducts, 'category', expect),
    });
  });
});
