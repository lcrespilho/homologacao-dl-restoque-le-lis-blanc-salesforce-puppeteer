const puppeteer = require('puppeteer');
const { default: expect } = require('expect-puppeteer');

const pages = {
  home: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/home-lelis',
  home_casa: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/home-casa',
  politicas_privacidade: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/politicas-regulamentos',
  category: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/lelis/new',
  search: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/pesquisa?search-button=&q=saia+couro&lang=pt_BR',
  products: [
    'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/lelis/moda/roupas/saias/p-saia-le-lis-blanc-andressa-feminina-16.03.0994.html?dwvar_16.03.0994_color=94&quantity=1', // produto disponível
    'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/casa/mesa/pratos-e-sousplats/p-prato-raso-le-lis-casa-figo-60.72.0215_94_1.html', // produto da LL-Casa disponível
    'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/p-regata-le-lis-petit-picol%C3%A9-feminina-11.06.6714.html', // produto indisponível
  ],
  cart: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/carrinho',
  checkout: {
    shipping: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/checkout?stage=shipping#shipping',
    payment: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/checkout?stage=payment#payment',
    resume: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/checkout?stage=placeOrder#placeOrder',
  },
  typ: 'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/orderconfirm',
  petit_acessorios_casa:
    'https://staging-na01-restoque.demandware.net/s/le_lis_blanc/lelis/petit/casa-petit/acessorios-casa-petit',
};

const selectors = {
  home: {},
  category: {
    listProducts: 'div.product',
  },
  search: {
    listProducts: 'div.product',
    filtrar: '.filter-button',
    filtrar_preto: 'button[data-href$="prefv1=Preto"]',
    filtrar_aquarela: 'button[data-href$="prefv2=AQUARELA"]',
  },
  product: {},
  cart: {},
  checkout: {},
  typ: {},
  generic: {
    aware: '.cc-compliance a',
    newsletter: '.icon-close.icon-close-newsletter',
    loginForm: '.modalLogin form',
    loginSubmit: '.modalLogin form button[type="submit"]',
  },
};

/**
 * Retorna um objeto com o produto principal da PDP.
 *
 * @param {puppeteer.Page} page
 * @param {jest.Expect} expect
 */
async function product_pdp_DOM(page, expect) {
  const product = await page.evaluate(async () => {
    try {
      var sku = document.querySelector('span.product-id').innerText.trim();
      var variant = sku;
    } catch (error) {}
    try {
      var id = sku.split('_')[0];
    } catch (error) {}
    try {
      var color = document.querySelector('.descriptionDetail__description-text').innerText.match('Cor: (.*)')?.[1];
    } catch (error) {}
    try {
      var name = document.querySelector('.product-name').textContent.trim();
    } catch (error) {}
    try {
      var priceFrom = +document.querySelector('.product-detail .attributes .list .value').getAttribute('content');
    } catch (e) {}
    try {
      var price = +document
        .querySelector('.product-detail .attributes .sales')
        .innerText.trim()
        .match(/R\$ (\d+(\.?\d+)+,\d+)/)[1]
        .replace(/\./g, '')
        .replace(',', '.');
    } catch (error) {}
    if (!sku.includes('_')) {
      sku = variant = 'PROVÁVEL ERRO DE REDE E/OU DOM';
    }
    try {
      var size = document.querySelector('#size-2 button[selected] > span').innerText;
    } catch (error) {}
    return {
      id,
      sku,
      variant,
      size,
      color,
      name,
      priceFrom,
      price,
      brand: 'LE LIS BLANC',
      list: 'product',
      position: 1,
      quantity: 1,
      presale: false,
      unavailable: !(await isProductAvailable(id)),
      gift: false,
      giftwrap: false,
      fromlook: false,
      quickshop: false,
    };
  });
  product.permalink = expect.any(String);
  product.category = expect.any(String);
  product.new = expect.any(Boolean);
  return product;
}

/**
 * Retorna um array com os produtos listados em páginas de busca e categoria.
 *
 * @param {puppeteer.Page} page
 * @param {string} css Seletor do produto individual em listagens. Em geral, 'div.product'
 * @return {*}
 */
async function listProducts_DOM(page, css, list, expect) {
  const listProducts = await page.evaluate(
    (css, list) => {
      return Promise.all(
        Array.from(document.querySelectorAll(css)).map(async (p, idx) => {
          let sku, variant;
          let id = p.dataset.pid;
          let permalink = p.querySelector('a').href;
          if (/_\d+_\d+/.test(id)) {
            sku = variant = id;
            id = id.replace(/_\d+_\d+/, '');
            permalink = permalink.replace(sku, id);
          }
          const name = p.querySelector('img').title;
          const price = +p
            .querySelector('span.sales')
            .innerText.trim()
            .replace('R$ ', '')
            .replace('.', '')
            .replace(',', '.');
          const priceFrom = +p.querySelector('span.list .value')?.getAttribute('content') || undefined;
          const brand = 'LE LIS BLANC'; // fixo, na falta de opção melhor
          const position = idx + 1;
          const quantity = 1; // para visualização de página deve ser sempre 1
          const presale = false; // não dá pra pegar do DOM, mas no DL vem sempre assim.
          const _new = false; // não dá para pegar do DOM, mas no DL vem sempre assim.
          const unavailable = !(await isProductAvailable(id)); // exposed
          const gift = false;
          const giftwrap = false;
          const fromlook = false;
          const quickshop = false;
          const ret = {
            id,
            permalink,
            name,
            priceFrom,
            price,
            brand,
            list,
            position,
            quantity,
            presale,
            new: _new,
            unavailable,
            gift,
            giftwrap,
            fromlook,
            quickshop,
          };
          if (sku) {
            ret.sku = sku;
            ret.variant = variant;
          }
          return ret;
        })
      );
    },
    css,
    list
  );
  listProducts.forEach(p => {
    p.category = expect.any(String); // porque em página de categoria essa propriedade pode variar por produto
  });
  return listProducts;
}

async function comprar(page, productPage) {}

/**
 * Efetua logout, e devolve a página na Home, totalmente carregada.
 *
 * @param {puppeteer.Page} page
 */
function logout(page) {
  return page.goto(
    'https://staging-na01-restoque.demandware.net/on/demandware.store/Sites-le_lis_blanc-Site/pt_BR/Login-Logout',
    {
      waitUntil: 'networkidle0',
    }
  );
}

/**
 * Loga no site, e devolve a página na Home, totalmente carregada.
 *
 * @param {puppeteer.Page} page
 * @param {string} username
 * @param {string} password
 * @example
 *  await login(page, 'tags.tests.2021113002@raccoon.ag', '$8rJdS7B#@mJYg');
 *  await login(page, 'tags.tests.202201071@raccoon.ag', 'tags.tests.202201071@raccoon.ag');
 */
async function login(page, username, password) {
  await page.goto(pages.home, { waitUntil: 'networkidle0' });
  await close_aware(page);
  await close_newsletter(page);
  await page.waitForTimeout(2000);
  await page.evaluate(() => {
    $('.modalDefaultMask, .modalLogin').fadeIn();
    $('.dropdown-login-pop').removeClass('show');
  });
  await page.waitForSelector(selectors.generic.loginForm, { visible: true });
  await expect(page).toFillForm(
    selectors.generic.loginForm,
    {
      loginEmail: username,
      loginPassword: password,
    },
    { delay: 40 }
  );
  await Promise.all([page.click(selectors.generic.loginSubmit), page.waitForNavigation({ waitUntil: 'networkidle0' })]);
}

/**
 * Clique no botão de fechamento do newsletter
 *
 * @param {puppeteer.Page} page
 */
async function close_newsletter(page) {
  try {
    await page
      .waitForSelector(selectors.generic.newsletter, { visible: true, timeout: 2000 })
      .then(newsletterButton => newsletterButton.click())
      .catch(() => {});
  } catch (e) {}
}

/**
 * Clique no botão "Aware"
 *
 * @param {puppeteer.Page} page
 */
async function close_aware(page) {
  try {
    await page
      .waitForSelector(selectors.generic.aware, { visible: true, timeout: 2000 })
      .then(consentButton => consentButton.click())
      .catch(() => {});
  } catch (e) {}
}

// export login funcion
module.exports = {
  login,
  pages,
  selectors,
  close_aware,
  close_newsletter,
  listProducts_DOM,
  product_pdp_DOM,
};
