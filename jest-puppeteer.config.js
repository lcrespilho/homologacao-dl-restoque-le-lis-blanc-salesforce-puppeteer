module.exports = {
  launch: {
    headless: process.env.HEADLESS !== 'false',
    devtools: process.env.DEVTOOLS === 'true',
    product: 'chrome',
    args: ['--no-sandbox', '--start-maximized'],
    defaultViewport: { width: 1800, height: 900 },
  },
  browserContext: 'default',
  exitOnPageError: false,
};
